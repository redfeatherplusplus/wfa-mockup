﻿namespace WFA_Control_Library
{
    partial class DimsUC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RemoveDimsButton = new System.Windows.Forms.Button();
            this.DimsBox3 = new System.Windows.Forms.ComboBox();
            this.DimsBox1 = new System.Windows.Forms.ComboBox();
            this.LogicBox2 = new System.Windows.Forms.ComboBox();
            this.DimsBox0 = new System.Windows.Forms.ComboBox();
            this.DimsBox2 = new System.Windows.Forms.ComboBox();
            this.LogicBox0 = new System.Windows.Forms.ComboBox();
            this.LogicBox1 = new System.Windows.Forms.ComboBox();
            this.GroupLogicBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // RemoveDimsButton
            // 
            this.RemoveDimsButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.RemoveDimsButton.Location = new System.Drawing.Point(577, 4);
            this.RemoveDimsButton.Name = "RemoveDimsButton";
            this.RemoveDimsButton.Size = new System.Drawing.Size(25, 21);
            this.RemoveDimsButton.TabIndex = 74;
            this.RemoveDimsButton.Text = "-";
            this.RemoveDimsButton.UseVisualStyleBackColor = true;
            this.RemoveDimsButton.Click += new System.EventHandler(this.RemoveDimsButton_Click);
            // 
            // DimsBox3
            // 
            this.DimsBox3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.DimsBox3.FormattingEnabled = true;
            this.DimsBox3.Location = new System.Drawing.Point(487, 4);
            this.DimsBox3.Name = "DimsBox3";
            this.DimsBox3.Size = new System.Drawing.Size(84, 21);
            this.DimsBox3.TabIndex = 73;
            // 
            // DimsBox1
            // 
            this.DimsBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.DimsBox1.FormattingEnabled = true;
            this.DimsBox1.Location = new System.Drawing.Point(206, 4);
            this.DimsBox1.Name = "DimsBox1";
            this.DimsBox1.Size = new System.Drawing.Size(84, 21);
            this.DimsBox1.TabIndex = 69;
            // 
            // LogicBox2
            // 
            this.LogicBox2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LogicBox2.FormattingEnabled = true;
            this.LogicBox2.Items.AddRange(new object[] {
            "--",
            "AND",
            "OR"});
            this.LogicBox2.Location = new System.Drawing.Point(437, 4);
            this.LogicBox2.Name = "LogicBox2";
            this.LogicBox2.Size = new System.Drawing.Size(45, 21);
            this.LogicBox2.TabIndex = 72;
            // 
            // DimsBox0
            // 
            this.DimsBox0.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.DimsBox0.FormattingEnabled = true;
            this.DimsBox0.Location = new System.Drawing.Point(65, 4);
            this.DimsBox0.Name = "DimsBox0";
            this.DimsBox0.Size = new System.Drawing.Size(84, 21);
            this.DimsBox0.TabIndex = 67;
            // 
            // DimsBox2
            // 
            this.DimsBox2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.DimsBox2.FormattingEnabled = true;
            this.DimsBox2.Location = new System.Drawing.Point(347, 4);
            this.DimsBox2.Name = "DimsBox2";
            this.DimsBox2.Size = new System.Drawing.Size(84, 21);
            this.DimsBox2.TabIndex = 71;
            // 
            // LogicBox0
            // 
            this.LogicBox0.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LogicBox0.FormattingEnabled = true;
            this.LogicBox0.Items.AddRange(new object[] {
            "--",
            "AND",
            "OR"});
            this.LogicBox0.Location = new System.Drawing.Point(155, 4);
            this.LogicBox0.Name = "LogicBox0";
            this.LogicBox0.Size = new System.Drawing.Size(45, 21);
            this.LogicBox0.TabIndex = 68;
            // 
            // LogicBox1
            // 
            this.LogicBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LogicBox1.FormattingEnabled = true;
            this.LogicBox1.Items.AddRange(new object[] {
            "--",
            "AND",
            "OR"});
            this.LogicBox1.Location = new System.Drawing.Point(296, 4);
            this.LogicBox1.Name = "LogicBox1";
            this.LogicBox1.Size = new System.Drawing.Size(45, 21);
            this.LogicBox1.TabIndex = 70;
            // 
            // GroupLogicBox
            // 
            this.GroupLogicBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.GroupLogicBox.FormattingEnabled = true;
            this.GroupLogicBox.Items.AddRange(new object[] {
            "--",
            "AND",
            "OR"});
            this.GroupLogicBox.Location = new System.Drawing.Point(3, 4);
            this.GroupLogicBox.Name = "GroupLogicBox";
            this.GroupLogicBox.Size = new System.Drawing.Size(56, 21);
            this.GroupLogicBox.TabIndex = 66;
            // 
            // DimsUC
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.RemoveDimsButton);
            this.Controls.Add(this.DimsBox3);
            this.Controls.Add(this.DimsBox1);
            this.Controls.Add(this.LogicBox2);
            this.Controls.Add(this.DimsBox0);
            this.Controls.Add(this.DimsBox2);
            this.Controls.Add(this.LogicBox0);
            this.Controls.Add(this.LogicBox1);
            this.Controls.Add(this.GroupLogicBox);
            this.Name = "DimsUC";
            this.Size = new System.Drawing.Size(605, 28);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button RemoveDimsButton;
        private System.Windows.Forms.ComboBox DimsBox3;
        private System.Windows.Forms.ComboBox DimsBox1;
        private System.Windows.Forms.ComboBox DimsBox0;
        private System.Windows.Forms.ComboBox DimsBox2;
        private System.Windows.Forms.ComboBox LogicBox0;
        private System.Windows.Forms.ComboBox LogicBox1;
        private System.Windows.Forms.ComboBox GroupLogicBox;
        private System.Windows.Forms.ComboBox LogicBox2;



    }
}
