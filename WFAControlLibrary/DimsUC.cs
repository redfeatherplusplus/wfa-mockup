﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFA_Control_Library
{
    public partial class DimsUC: UserControl
    {
        public DimsUC()
        {
            InitializeComponent();
            
            //initialize combo boxes
            GroupLogicBox.SelectedIndex = 0;
            LogicBox0.SelectedIndex = 0;
            LogicBox1.SelectedIndex = 0;
            LogicBox2.SelectedIndex = 0;
        }

        public void setDataSource(List<String> columns) {
            foreach (string column in columns)
            {
                DimsBox0.Items.Add(column);
                DimsBox1.Items.Add(column);
                DimsBox2.Items.Add(column);
                DimsBox3.Items.Add(column);
            }
        }

        private void RemoveDimsButton_Click(object sender, EventArgs e)
        {
            Parent.Controls.Remove(this);
        }

    }
}
