﻿namespace WFA_Project
{
    partial class WFAApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CSVSelection = new System.Windows.Forms.GroupBox();
            this.SelectActivityCSVButton = new System.Windows.Forms.Button();
            this.SelectEndCSVButton = new System.Windows.Forms.Button();
            this.SelectStartCSVButton = new System.Windows.Forms.Button();
            this.Metric = new System.Windows.Forms.Label();
            this.MetricBox = new System.Windows.Forms.ComboBox();
            this.DimsSelection = new System.Windows.Forms.GroupBox();
            this.DimsFlowLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.AddDimsButton = new System.Windows.Forms.Button();
            this.Dims = new System.Windows.Forms.Label();
            this.DimsBox3 = new System.Windows.Forms.ComboBox();
            this.DimsBox1 = new System.Windows.Forms.ComboBox();
            this.LogicBox2 = new System.Windows.Forms.ComboBox();
            this.DimsBox0 = new System.Windows.Forms.ComboBox();
            this.DimsBox2 = new System.Windows.Forms.ComboBox();
            this.LogicBox0 = new System.Windows.Forms.ComboBox();
            this.LogicBox1 = new System.Windows.Forms.ComboBox();
            this.TrendReportTextBox = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SumMetricStart = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.CalculateTrendButton = new System.Windows.Forms.Button();
            this.SaveTrendButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.AverageActivity = new System.Windows.Forms.MaskedTextBox();
            this.AverageEnd = new System.Windows.Forms.MaskedTextBox();
            this.AverageStart = new System.Windows.Forms.MaskedTextBox();
            this.SumDimsActivity = new System.Windows.Forms.MaskedTextBox();
            this.SumDimsEnd = new System.Windows.Forms.MaskedTextBox();
            this.SumDimsStart = new System.Windows.Forms.MaskedTextBox();
            this.SumMetricActivity = new System.Windows.Forms.MaskedTextBox();
            this.SumMetricEnd = new System.Windows.Forms.MaskedTextBox();
            this.CSVSelection.SuspendLayout();
            this.DimsSelection.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // CSVSelection
            // 
            this.CSVSelection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.CSVSelection.Controls.Add(this.SelectActivityCSVButton);
            this.CSVSelection.Controls.Add(this.SelectEndCSVButton);
            this.CSVSelection.Controls.Add(this.SelectStartCSVButton);
            this.CSVSelection.Location = new System.Drawing.Point(12, 33);
            this.CSVSelection.Name = "CSVSelection";
            this.CSVSelection.Size = new System.Drawing.Size(225, 163);
            this.CSVSelection.TabIndex = 0;
            this.CSVSelection.TabStop = false;
            this.CSVSelection.Text = "Select CSV";
            // 
            // SelectActivityCSVButton
            // 
            this.SelectActivityCSVButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SelectActivityCSVButton.Location = new System.Drawing.Point(6, 104);
            this.SelectActivityCSVButton.Name = "SelectActivityCSVButton";
            this.SelectActivityCSVButton.Size = new System.Drawing.Size(212, 23);
            this.SelectActivityCSVButton.TabIndex = 2;
            this.SelectActivityCSVButton.Text = "Select Activity CSV";
            this.SelectActivityCSVButton.UseVisualStyleBackColor = true;
            this.SelectActivityCSVButton.Click += new System.EventHandler(this.SelectActivityCSVButton_Click);
            // 
            // SelectEndCSVButton
            // 
            this.SelectEndCSVButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SelectEndCSVButton.Location = new System.Drawing.Point(6, 76);
            this.SelectEndCSVButton.Name = "SelectEndCSVButton";
            this.SelectEndCSVButton.Size = new System.Drawing.Size(213, 23);
            this.SelectEndCSVButton.TabIndex = 1;
            this.SelectEndCSVButton.Text = "Select End CSV";
            this.SelectEndCSVButton.UseVisualStyleBackColor = true;
            this.SelectEndCSVButton.Click += new System.EventHandler(this.SelectEndCSVButton_Click);
            // 
            // SelectStartCSVButton
            // 
            this.SelectStartCSVButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SelectStartCSVButton.Location = new System.Drawing.Point(6, 47);
            this.SelectStartCSVButton.Name = "SelectStartCSVButton";
            this.SelectStartCSVButton.Size = new System.Drawing.Size(213, 23);
            this.SelectStartCSVButton.TabIndex = 0;
            this.SelectStartCSVButton.Text = "Select Start CSV";
            this.SelectStartCSVButton.UseVisualStyleBackColor = true;
            this.SelectStartCSVButton.Click += new System.EventHandler(this.SelectStartCSVButton_Click);
            // 
            // Metric
            // 
            this.Metric.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Metric.AutoSize = true;
            this.Metric.Location = new System.Drawing.Point(12, 9);
            this.Metric.Name = "Metric";
            this.Metric.Size = new System.Drawing.Size(42, 13);
            this.Metric.TabIndex = 1;
            this.Metric.Text = "Metric: ";
            // 
            // MetricBox
            // 
            this.MetricBox.FormattingEnabled = true;
            this.MetricBox.Location = new System.Drawing.Point(60, 6);
            this.MetricBox.Name = "MetricBox";
            this.MetricBox.Size = new System.Drawing.Size(177, 21);
            this.MetricBox.TabIndex = 2;
            // 
            // DimsSelection
            // 
            this.DimsSelection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DimsSelection.Controls.Add(this.DimsFlowLayout);
            this.DimsSelection.Controls.Add(this.AddDimsButton);
            this.DimsSelection.Controls.Add(this.Dims);
            this.DimsSelection.Controls.Add(this.DimsBox3);
            this.DimsSelection.Controls.Add(this.DimsBox1);
            this.DimsSelection.Controls.Add(this.LogicBox2);
            this.DimsSelection.Controls.Add(this.DimsBox0);
            this.DimsSelection.Controls.Add(this.DimsBox2);
            this.DimsSelection.Controls.Add(this.LogicBox0);
            this.DimsSelection.Controls.Add(this.LogicBox1);
            this.DimsSelection.Location = new System.Drawing.Point(243, 6);
            this.DimsSelection.Name = "DimsSelection";
            this.DimsSelection.Size = new System.Drawing.Size(620, 190);
            this.DimsSelection.TabIndex = 3;
            this.DimsSelection.TabStop = false;
            this.DimsSelection.Text = "Dimensions Selection";
            // 
            // DimsFlowLayout
            // 
            this.DimsFlowLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.DimsFlowLayout.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.DimsFlowLayout.Location = new System.Drawing.Point(9, 40);
            this.DimsFlowLayout.Name = "DimsFlowLayout";
            this.DimsFlowLayout.Size = new System.Drawing.Size(605, 144);
            this.DimsFlowLayout.TabIndex = 14;
            // 
            // AddDimsButton
            // 
            this.AddDimsButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.AddDimsButton.Location = new System.Drawing.Point(589, 13);
            this.AddDimsButton.Name = "AddDimsButton";
            this.AddDimsButton.Size = new System.Drawing.Size(25, 21);
            this.AddDimsButton.TabIndex = 48;
            this.AddDimsButton.Text = "+";
            this.AddDimsButton.UseVisualStyleBackColor = true;
            this.AddDimsButton.Click += new System.EventHandler(this.AddDimsButton_Click);
            // 
            // Dims
            // 
            this.Dims.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Dims.AutoSize = true;
            this.Dims.Location = new System.Drawing.Point(6, 16);
            this.Dims.Name = "Dims";
            this.Dims.Size = new System.Drawing.Size(67, 13);
            this.Dims.TabIndex = 0;
            this.Dims.Text = "Dimensions: ";
            // 
            // DimsBox3
            // 
            this.DimsBox3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.DimsBox3.FormattingEnabled = true;
            this.DimsBox3.Location = new System.Drawing.Point(499, 13);
            this.DimsBox3.Name = "DimsBox3";
            this.DimsBox3.Size = new System.Drawing.Size(84, 21);
            this.DimsBox3.TabIndex = 47;
            // 
            // DimsBox1
            // 
            this.DimsBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.DimsBox1.FormattingEnabled = true;
            this.DimsBox1.Location = new System.Drawing.Point(218, 13);
            this.DimsBox1.Name = "DimsBox1";
            this.DimsBox1.Size = new System.Drawing.Size(84, 21);
            this.DimsBox1.TabIndex = 43;
            // 
            // LogicBox2
            // 
            this.LogicBox2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LogicBox2.FormattingEnabled = true;
            this.LogicBox2.Items.AddRange(new object[] {
            "--",
            "AND",
            "OR"});
            this.LogicBox2.Location = new System.Drawing.Point(449, 13);
            this.LogicBox2.Name = "LogicBox2";
            this.LogicBox2.Size = new System.Drawing.Size(45, 21);
            this.LogicBox2.TabIndex = 46;
            // 
            // DimsBox0
            // 
            this.DimsBox0.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.DimsBox0.FormattingEnabled = true;
            this.DimsBox0.Location = new System.Drawing.Point(77, 13);
            this.DimsBox0.Name = "DimsBox0";
            this.DimsBox0.Size = new System.Drawing.Size(84, 21);
            this.DimsBox0.TabIndex = 41;
            // 
            // DimsBox2
            // 
            this.DimsBox2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.DimsBox2.FormattingEnabled = true;
            this.DimsBox2.Location = new System.Drawing.Point(359, 13);
            this.DimsBox2.Name = "DimsBox2";
            this.DimsBox2.Size = new System.Drawing.Size(84, 21);
            this.DimsBox2.TabIndex = 45;
            // 
            // LogicBox0
            // 
            this.LogicBox0.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LogicBox0.FormattingEnabled = true;
            this.LogicBox0.Items.AddRange(new object[] {
            "--",
            "AND",
            "OR"});
            this.LogicBox0.Location = new System.Drawing.Point(167, 13);
            this.LogicBox0.Name = "LogicBox0";
            this.LogicBox0.Size = new System.Drawing.Size(45, 21);
            this.LogicBox0.TabIndex = 42;
            // 
            // LogicBox1
            // 
            this.LogicBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LogicBox1.FormattingEnabled = true;
            this.LogicBox1.Items.AddRange(new object[] {
            "--",
            "AND",
            "OR"});
            this.LogicBox1.Location = new System.Drawing.Point(308, 13);
            this.LogicBox1.Name = "LogicBox1";
            this.LogicBox1.Size = new System.Drawing.Size(45, 21);
            this.LogicBox1.TabIndex = 44;
            // 
            // TrendReportTextBox
            // 
            this.TrendReportTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TrendReportTextBox.Location = new System.Drawing.Point(231, 16);
            this.TrendReportTextBox.Name = "TrendReportTextBox";
            this.TrendReportTextBox.Size = new System.Drawing.Size(614, 88);
            this.TrendReportTextBox.TabIndex = 15;
            this.TrendReportTextBox.Text = "";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Sum Metric: ";
            // 
            // SumMetricStart
            // 
            this.SumMetricStart.Location = new System.Drawing.Point(78, 32);
            this.SumMetricStart.Name = "SumMetricStart";
            this.SumMetricStart.Size = new System.Drawing.Size(45, 20);
            this.SumMetricStart.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Sum Dims:  ";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Average:  ";
            // 
            // CalculateTrendButton
            // 
            this.CalculateTrendButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CalculateTrendButton.Cursor = System.Windows.Forms.Cursors.Default;
            this.CalculateTrendButton.Location = new System.Drawing.Point(12, 319);
            this.CalculateTrendButton.Name = "CalculateTrendButton";
            this.CalculateTrendButton.Size = new System.Drawing.Size(225, 26);
            this.CalculateTrendButton.TabIndex = 22;
            this.CalculateTrendButton.Text = "Calculate";
            this.CalculateTrendButton.UseVisualStyleBackColor = true;
            this.CalculateTrendButton.Click += new System.EventHandler(this.CalculateTrendButton_Click);
            // 
            // SaveTrendButton
            // 
            this.SaveTrendButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SaveTrendButton.Location = new System.Drawing.Point(243, 319);
            this.SaveTrendButton.Name = "SaveTrendButton";
            this.SaveTrendButton.Size = new System.Drawing.Size(620, 26);
            this.SaveTrendButton.TabIndex = 23;
            this.SaveTrendButton.Text = "Save";
            this.SaveTrendButton.UseVisualStyleBackColor = true;
            this.SaveTrendButton.Click += new System.EventHandler(this.SaveTrendButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.AverageActivity);
            this.groupBox1.Controls.Add(this.AverageEnd);
            this.groupBox1.Controls.Add(this.AverageStart);
            this.groupBox1.Controls.Add(this.SumDimsActivity);
            this.groupBox1.Controls.Add(this.SumDimsEnd);
            this.groupBox1.Controls.Add(this.SumDimsStart);
            this.groupBox1.Controls.Add(this.SumMetricActivity);
            this.groupBox1.Controls.Add(this.SumMetricEnd);
            this.groupBox1.Controls.Add(this.TrendReportTextBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.SumMetricStart);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 202);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(851, 111);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Trend Data";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(177, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 32;
            this.label6.Text = "Activity";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(126, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "End";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(75, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 30;
            this.label4.Text = "Start ";
            // 
            // AverageActivity
            // 
            this.AverageActivity.Location = new System.Drawing.Point(180, 84);
            this.AverageActivity.Name = "AverageActivity";
            this.AverageActivity.Size = new System.Drawing.Size(45, 20);
            this.AverageActivity.TabIndex = 29;
            // 
            // AverageEnd
            // 
            this.AverageEnd.Location = new System.Drawing.Point(129, 84);
            this.AverageEnd.Name = "AverageEnd";
            this.AverageEnd.Size = new System.Drawing.Size(45, 20);
            this.AverageEnd.TabIndex = 28;
            // 
            // AverageStart
            // 
            this.AverageStart.Location = new System.Drawing.Point(78, 84);
            this.AverageStart.Name = "AverageStart";
            this.AverageStart.Size = new System.Drawing.Size(45, 20);
            this.AverageStart.TabIndex = 27;
            // 
            // SumDimsActivity
            // 
            this.SumDimsActivity.Location = new System.Drawing.Point(180, 58);
            this.SumDimsActivity.Name = "SumDimsActivity";
            this.SumDimsActivity.Size = new System.Drawing.Size(45, 20);
            this.SumDimsActivity.TabIndex = 26;
            // 
            // SumDimsEnd
            // 
            this.SumDimsEnd.Location = new System.Drawing.Point(129, 58);
            this.SumDimsEnd.Name = "SumDimsEnd";
            this.SumDimsEnd.Size = new System.Drawing.Size(45, 20);
            this.SumDimsEnd.TabIndex = 25;
            // 
            // SumDimsStart
            // 
            this.SumDimsStart.Location = new System.Drawing.Point(78, 58);
            this.SumDimsStart.Name = "SumDimsStart";
            this.SumDimsStart.Size = new System.Drawing.Size(45, 20);
            this.SumDimsStart.TabIndex = 24;
            // 
            // SumMetricActivity
            // 
            this.SumMetricActivity.Location = new System.Drawing.Point(180, 32);
            this.SumMetricActivity.Name = "SumMetricActivity";
            this.SumMetricActivity.Size = new System.Drawing.Size(45, 20);
            this.SumMetricActivity.TabIndex = 23;
            // 
            // SumMetricEnd
            // 
            this.SumMetricEnd.Location = new System.Drawing.Point(129, 32);
            this.SumMetricEnd.Name = "SumMetricEnd";
            this.SumMetricEnd.Size = new System.Drawing.Size(45, 20);
            this.SumMetricEnd.TabIndex = 22;
            // 
            // WFAApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(869, 352);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.SaveTrendButton);
            this.Controls.Add(this.CalculateTrendButton);
            this.Controls.Add(this.DimsSelection);
            this.Controls.Add(this.MetricBox);
            this.Controls.Add(this.Metric);
            this.Controls.Add(this.CSVSelection);
            this.Name = "WFAApp";
            this.Text = "WFA Project Mockup";
            this.CSVSelection.ResumeLayout(false);
            this.DimsSelection.ResumeLayout(false);
            this.DimsSelection.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox CSVSelection;
        private System.Windows.Forms.Label Metric;
        private System.Windows.Forms.ComboBox MetricBox;
        private System.Windows.Forms.GroupBox DimsSelection;
        private System.Windows.Forms.Button SelectActivityCSVButton;
        private System.Windows.Forms.Button SelectEndCSVButton;
        private System.Windows.Forms.RichTextBox TrendReportTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox SumMetricStart;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button SaveTrendButton;
        private System.Windows.Forms.Button CalculateTrendButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label Dims;
        private System.Windows.Forms.MaskedTextBox SumMetricActivity;
        private System.Windows.Forms.MaskedTextBox SumMetricEnd;
        private System.Windows.Forms.MaskedTextBox AverageActivity;
        private System.Windows.Forms.MaskedTextBox AverageEnd;
        private System.Windows.Forms.MaskedTextBox AverageStart;
        private System.Windows.Forms.MaskedTextBox SumDimsActivity;
        private System.Windows.Forms.MaskedTextBox SumDimsEnd;
        private System.Windows.Forms.MaskedTextBox SumDimsStart;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button SelectStartCSVButton;
        private System.Windows.Forms.FlowLayoutPanel DimsFlowLayout;
        private System.Windows.Forms.Button AddDimsButton;
        private System.Windows.Forms.ComboBox DimsBox3;
        private System.Windows.Forms.ComboBox DimsBox1;
        private System.Windows.Forms.ComboBox LogicBox2;
        private System.Windows.Forms.ComboBox DimsBox0;
        private System.Windows.Forms.ComboBox DimsBox2;
        private System.Windows.Forms.ComboBox LogicBox0;
        private System.Windows.Forms.ComboBox LogicBox1;
    }
}

