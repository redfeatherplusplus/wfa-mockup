﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFA_Control_Library;
using WFA_Tech_Services;

namespace WFA_Project
{
    public partial class WFAApp : Form
    {
        //csv reader
        private CSVReader reader;

        //csv file names and locations
        private string StartDataFile;
        private string EndDataFile;
        private string ActivityDataFile;
        private WFAData StartData;
        private WFAData EndData;
        private WFAData ActivityData;

        //column data parameters
        private List<String> columns;

        //sum of dims and metrics
        private double SumMetricStartVal;
        private double SumMetricEndVal;
        private double SumMetricActivityVal;
        private double SumDimsStartVal;
        private double SumDimsEndVal;
        private double SumDimsActivityVal;

        public WFAApp()
        {
            InitializeComponent();

            //initialize CSV reader
            reader = new CSVReader();

            //initialize combo boxes and their data sources
            LogicBox0.SelectedIndex = 0;
            LogicBox1.SelectedIndex = 0;
            LogicBox2.SelectedIndex = 0;

            //initialize sum values
            SumMetricStartVal = 0;
            SumMetricEndVal = 0;
            SumMetricActivityVal = 0;
            SumDimsStartVal = 0;
            SumDimsEndVal = 0;
            SumDimsActivityVal = 0;
        }

        private void AddDimsButton_Click(object sender, EventArgs e)
        {
            if (columns == null)
            {
                System.Windows.Forms.MessageBox.Show("Please select CSV files first.");
            }
            else
            {
                DimsUC DimsControl = new DimsUC();
                DimsControl.setDataSource(columns);
                DimsControl.Parent = DimsFlowLayout;
                DimsFlowLayout.Controls.Add(DimsControl);
            }
        }

        private void SelectStartCSVButton_Click(object sender, EventArgs e)
        {
            //try to read in the start csv file
            try
            {
                StartData = reader.readCSV(out StartDataFile);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.ToString());
            }

            //disable current button
            SelectStartCSVButton.Enabled = false;

            //set universal data source if all files have been selected
            if (!SelectStartCSVButton.Enabled && !SelectEndCSVButton.Enabled && !SelectActivityCSVButton.Enabled)
            {
                columns = StartData.Columns;
                foreach (string column in columns)
                {
                    MetricBox.Items.Add(column);
                    DimsBox0.Items.Add(column);
                    DimsBox1.Items.Add(column);
                    DimsBox2.Items.Add(column);
                    DimsBox3.Items.Add(column);
                }
            }
        }

        private void SelectEndCSVButton_Click(object sender, EventArgs e)
        {
            //try to read in the end csv file
            try
            {
                EndData = reader.readCSV(out EndDataFile);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.ToString());
            }

            //disable current button
            SelectEndCSVButton.Enabled = false;

            //set universal data source if all files have been selected
            if (!SelectStartCSVButton.Enabled && !SelectEndCSVButton.Enabled && !SelectActivityCSVButton.Enabled)
            {
                columns = StartData.Columns;
                foreach (string column in columns)
                {
                    MetricBox.Items.Add(column);
                    DimsBox0.Items.Add(column);
                    DimsBox1.Items.Add(column);
                    DimsBox2.Items.Add(column);
                    DimsBox3.Items.Add(column);
                }
            }
        }

        private void SelectActivityCSVButton_Click(object sender, EventArgs e)
        {
            //try to read in the activity csv file
            try
            {
                ActivityData = reader.readCSV(out ActivityDataFile);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.ToString());
            }

            //disable current button
            SelectActivityCSVButton.Enabled = false;

            //set universal data source if all files have been selected
            setColumnSource();
        }

        private void CalculateTrendButton_Click(object sender, EventArgs e)
        {
            if (columns == null)
            {
                System.Windows.Forms.MessageBox.Show("Please select CSV files first.");
            }
            else 
            {
                //calculate sums of dims and metrics
                calculateSums();

                //update sum/average boxes 
                SumMetricStart.Text = SumMetricStartVal.ToString();
                SumMetricEnd.Text = SumMetricEndVal.ToString();
                SumMetricActivity.Text = SumMetricActivityVal.ToString();
                SumDimsStart.Text = SumDimsStartVal.ToString();
                SumDimsEnd.Text = SumDimsEndVal.ToString();
                SumDimsActivity.Text = SumDimsActivityVal.ToString();
                AverageStart.Text = Math.Round((SumDimsStartVal / SumMetricStartVal), 2).ToString();
                AverageEnd.Text = Math.Round((SumDimsEndVal / SumMetricEndVal), 2).ToString();
                AverageActivity.Text = Math.Round((SumDimsActivityVal / SumMetricActivityVal), 2).ToString();

                //update trend text field
                String trend = "Trend Report: " + "\n";
                trend += "Date Generated: " + DateTime.Today.ToShortDateString() + "\n";
                trend += "Start Period: " + StartDataFile + "\n";
                trend += "End Period: " + EndDataFile + "\n";
                trend += "Activity Period: " + ActivityDataFile + "\n";
                trend += "\n";

                //include metric sums
                trend += "Sum of Metric \"" + StartData.Columns[MetricBox.SelectedIndex]
                    + "\" is " + SumMetricStart.Text + " for start period." + "\n";
                trend += "Sum of Metric \"" + EndData.Columns[MetricBox.SelectedIndex]
                    + "\" is " + SumMetricEnd.Text + " for end period." + "\n";
                trend += "Sum of Metric \"" + ActivityData.Columns[MetricBox.SelectedIndex]
                    + "\" is " + SumMetricActivity.Text + " for activity period." + "\n";
                trend += "\n";

                //include dim sums (Yum!)
                trend += "Sum of Dims \"" + StartData.Columns[DimsBox0.SelectedIndex]
                    + "\" is " + SumDimsStart.Text + " for start period." + "\n";
                trend += "Sum of Dims \"" + EndData.Columns[DimsBox0.SelectedIndex]
                    + "\" is " + SumDimsEnd.Text + " for end period." + "\n";
                trend += "Sum of Dims \"" + ActivityData.Columns[DimsBox0.SelectedIndex]
                    + "\" is " + SumDimsActivity.Text + " for activity period." + "\n";
                trend += "\n";

                //include averages
                trend += "Average of \"" + StartData.Columns[DimsBox0.SelectedIndex]
                    + " / " + StartData.Columns[MetricBox.SelectedIndex]
                    + "\" is " + AverageStart.Text + " for start period." + "\n";
                trend += "Average of \"" + EndData.Columns[DimsBox0.SelectedIndex]
                    + " / " + EndData.Columns[MetricBox.SelectedIndex]
                    + "\" is " + AverageEnd.Text + " for end period." + "\n";
                trend += "Average of \"" + ActivityData.Columns[DimsBox0.SelectedIndex]
                    + " / " + ActivityData.Columns[MetricBox.SelectedIndex]
                    + "\" is " + AverageActivity.Text + " for activity period." + "\n";
                
                //update text box
                TrendReportTextBox.Text = trend;
            }
        }

        private void SaveTrendButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(sfd.FileName, TrendReportTextBox.Text);
            }  
        }

        private void setColumnSource()
        {
            if (!SelectStartCSVButton.Enabled && !SelectEndCSVButton.Enabled && !SelectActivityCSVButton.Enabled)
            {
                //clean each data set to ensure that they share the same columns
                columns = StartData.Columns;

                //for all columns check for extra columns
                for (int i = 0; i < columns.Count; i++)
                {
                    if (columns[i] != ActivityData.Columns[i])
                    {
                        //extra column found, remove column, step back one, then continue checking
                        ActivityData.Columns.RemoveAt(i);
                        ActivityData.Entries.RemoveAt(i);
                        i--;
                    }
                }

                //add each column to the metric and dim boxes
                foreach (string column in columns)
                {
                    MetricBox.Items.Add(column);
                    DimsBox0.Items.Add(column);
                    DimsBox1.Items.Add(column);
                    DimsBox2.Items.Add(column);
                    DimsBox3.Items.Add(column);
                }
            }
        }

        private void calculateSums()
        {
            double value;  //temp value used as an out variable

            //re-initialize sum values
            SumMetricStartVal = 0;
            SumMetricEndVal = 0;
            SumMetricActivityVal = 0;
            SumDimsStartVal = 0;
            SumDimsEndVal = 0;
            SumDimsActivityVal = 0;

            //calculate sum of entries for the metric column
            foreach (string entry in StartData.Entries[MetricBox.SelectedIndex])
            {
                //check if the entry is numeric
                if (double.TryParse(entry, out value))
                {
                    SumMetricStartVal += value;
                }
            }

            //calculate sum of entries for the metric column
            foreach (string entry in EndData.Entries[MetricBox.SelectedIndex])
            {
                //check if the entry is numeric
                if (double.TryParse(entry, out value))
                {
                    SumMetricEndVal += value;
                }
            }

            //calculate sum of entries for the metric column
            foreach (string entry in ActivityData.Entries[MetricBox.SelectedIndex])
            {
                //check if the entry is numeric
                if (double.TryParse(entry, out value))
                {
                    SumMetricActivityVal += value;
                }
            }

            //calculate sum of entries for the metric column
            foreach (string entry in StartData.Entries[DimsBox0.SelectedIndex])
            {
                //check if the entry is numeric
                if (double.TryParse(entry, out value))
                {
                    SumDimsStartVal += value;
                }
            }

            //calculate sum of entries for the metric column
            foreach (string entry in EndData.Entries[DimsBox0.SelectedIndex])
            {
                //check if the entry is numeric
                if (double.TryParse(entry, out value))
                {
                    SumDimsEndVal += value;
                }
            }

            //calculate sum of entries for the metric column
            foreach (string entry in ActivityData.Entries[DimsBox0.SelectedIndex])
            {
                //check if the entry is numeric
                if (double.TryParse(entry, out value))
                {
                    SumDimsActivityVal += value;
                }
            }
        }
    }
}