﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WFA_Tech_Services
{
    public class CSVReader : Form
    {
        public WFAData readCSV(out string CSVFileName) {
            WFAData data = new WFAData();  //data to return

            //open a window to let the user select a CSV file
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "CSV Files|*.csv";
            if (ofd.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                //get every line of the csv file
                CSVFileName = ofd.FileName;
                string[] lines = File.ReadAllLines(CSVFileName);
                    
                //first line contains collumn names
                string[] columns = lines[0].Split(',');

                //put each column into the WFAData
                //and create a list for that column's data
                foreach (string column in columns)
                {
                    data.Columns.Add(column);
                    data.Entries.Add(new List<String>());
                }

                //put each entry into the appropriate column
                for (int i = 1; i < lines.Length; i++)
                {
                    string[] line = lines[i].Split(',');
                    for (int j = 0; j < columns.Length; j++)
                    {
                        data.Entries[j].Add(line[j]);
                    }
                }

                return(data);
            }
            else {
                throw new System.Exception("Error reading or opening CSV file.");
            }
        }
    }
}
