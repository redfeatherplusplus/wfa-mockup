﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFA_Tech_Services
{
    public class WFAData
    {
        private List<String> columns;        //name of each column
        private List<List<String>> entries;  //entries of each columns

        //WFAData Constructor
        public WFAData()
        {
            columns = new List<String>();
            entries = new List<List<String>>();
        }

        //getters and setters
        public List<String> Columns
        {
            get { return columns; }
            set { columns = value; }
        }
        public List<List<String>> Entries
        {
            get { return entries; }
            set { entries = value; }
        }
    }
}
